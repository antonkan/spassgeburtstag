import 'dart:math';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:spassgeburtstag/utils.dart';

class Model {
  static const String FINAL_KEY = "final";

  final Map<String, QuestionData> questions = Map<String, QuestionData>();

  final List<QuestionData> _questionsList = [
    QuestionData(
      questionKey: "200718", //erstes Mal zusammen feiern
      questionText: "Wo haben wir uns zum ersten Mal geküsst?",
      questionImageUri: 'images/kater_blau.jpg',
      correctAnswer: "Kater Blau",
      wrongAnswerMessage: "Daran erinnerst du dich nicht mehr? Tsss :P",
      successMessage:
          "Richtig, das war im Kater Blau! Was für eine Erinnerung...",
      answerImageUri: 'images/mona_lisa.jpg',
    ),
    QuestionData(
      questionKey: "090718", //erstes "date"
      questionText: "Welcher Modus wird hier dargestellt? (Fachbegriff)",
      questionImageUri: 'images/jammolodus.jpeg',
      correctAnswer: "Jammolodus",
      wrongAnswerMessage: "Na, das solltest du doch besser wissen! :P",
      successMessage: "Richtig! Und in dem Modus sind wir eigentlich immer!",
      answerImageUri: 'images/emma_watson.jpg',
    ),
    QuestionData(
      questionKey: "080218", //als ich dich das erste Mal gesehen habe
      questionText: "Welcher wundervolle Künstler wird hier gezeigt?",
      questionImageUri: 'images/monolink.jpg',
      correctAnswer: "Monolink",
      wrongAnswerMessage: "Ohje! Kleiner Tipp: Ihm ist häufiger mal kalt!",
      successMessage: "Richtig! Ich habe direkt seine Stimme im Ohr :)",
      answerImageUri: 'images/mickey_mouse.jpg',
    ),
    QuestionData(
      questionKey: "200918", //when it all began
      questionText:
          "Egal, wie der Abend war, wem hat's auf jeden Fall gefallen?",
      questionImageUri: 'images/wilde_renate.jpg',
      correctAnswer: "Renate",
      wrongAnswerMessage:
          "Ohje! Kleiner Tipp: Sie hat ihren ganz eigenen, wilden Salon!",
      successMessage: "Richtig! Renaaaate hat's gefallen!",
      answerImageUri: 'images/paris_hilton.jpg',
    )
  ];
  final String finalAnswer = "Paris";
  final String finalImageUri = 'images/saskia_anton.jpg';

  bool _finalQuestionComplete = false;
  int? _daysLeftUntilTarget;

  Model() {
    _questionsList.forEach((element) {
      questions[element.questionKey] = element;
    });
  }

  Future<void> init() async {
    for (QuestionData question in questions.values) {
      await question.init();
    }
    _finalQuestionComplete = await _getPersistedFinalQuestionComplete();
    await Future.delayed(Duration(milliseconds: 5700));
  }

  bool finalAnswerCorrect(String input) {
    return simplifyString(input) == simplifyString(finalAnswer);
  }

  bool allAnswersComplete() =>
      !questions.values.any((element) => !element.getIsComplete());

  Future<void> resetAllAnswers() async {
    for (QuestionData question in questions.values) {
      await question.setIsComplete(false);
    }
    await setFinalQuestionComplete(false);
  }

  Future<bool> _getPersistedFinalQuestionComplete() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(FINAL_KEY)) {
      return prefs.getBool(FINAL_KEY)!;
    } else {
      prefs.setBool(FINAL_KEY, false);
      return false;
    }
  }

  bool getFinalQuestionComplete() {
    return _finalQuestionComplete;
  }

  Future<void> setFinalQuestionComplete(bool value) async {
    _finalQuestionComplete = value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(FINAL_KEY, value);
  }

  int _calculateDaysLeftUntilTarget() {
    var now = DateTime.now().millisecondsSinceEpoch / 1000;
    var target = 1619999999;
    var differenceInSec = target - now;
    var daysLeft = differenceInSec / (60 * 60 * 24);
    return max(daysLeft.toInt(), -1);
  }

  int getDaysLeftUntilTarget() {
    if (_daysLeftUntilTarget == null) {
      _daysLeftUntilTarget = _calculateDaysLeftUntilTarget();
    }
    return _daysLeftUntilTarget!;
  }

  bool getIsCounterVisible() {
    return _daysLeftUntilTarget != null && _daysLeftUntilTarget! > 0;
  }
}

class QuestionData {
  bool _isComplete = false;

  String questionKey;

  String questionImageUri;
  String questionText;
  String correctAnswer;

  String wrongAnswerMessage;
  String successMessage;
  String answerImageUri;

  QuestionData(
      {required this.questionKey,
      required this.questionImageUri,
      required this.questionText,
      required this.correctAnswer,
      required this.wrongAnswerMessage,
      required this.successMessage,
      required this.answerImageUri});

  bool isCorrectAnswer(String input) {
    return simplifyString(input) == simplifyString(correctAnswer);
  }

  Future<void> init() async {
    this._isComplete = await getPersistedIsComplete();
  }

  Future<bool> getPersistedIsComplete() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(questionKey)) {
      return prefs.getBool(questionKey)!;
    } else {
      prefs.setBool(questionKey, false);
      return false;
    }
  }

  bool getIsComplete() {
    return _isComplete;
  }

  Future<void> setIsComplete(bool value) async {
    _isComplete = value;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(questionKey, value);
  }
}

import 'package:flutter/material.dart';
import 'package:spassgeburtstag/data.dart';
import 'package:spassgeburtstag/final_answer_page.dart';
import 'package:spassgeburtstag/final_question_page.dart';
import 'package:spassgeburtstag/pin_input_page.dart';
import 'package:spassgeburtstag/styles.dart';

class SplashScreen extends StatefulWidget {
  final Model model;

  const SplashScreen({Key? key, required this.model}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState(model);
}

class _SplashScreenState extends State<SplashScreen> {
  final Model model;

  _SplashScreenState(this.model);

  @override
  void initState() {
    super.initState();
    model.init().then((value) => _navigate());
  }

  void _navigate() {
    Widget target;
    if (model.getFinalQuestionComplete()) {
      target = FinalAnswerPage(model: model);
    } else if (model.allAnswersComplete()) {
      target = FinalQuestionPage(model: model);
    } else {
      target = PinInputPage(model: model);
    }
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => target),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      child: Center(
        child: Image(
          image: AssetImage("images/lama_farting.gif"),
        ),
      ),
    );
  }
}

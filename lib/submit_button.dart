import 'package:flutter/material.dart';
import 'package:spassgeburtstag/styles.dart';

class SubmitButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String text;
  final double textSize;

  const SubmitButton(
      {Key? key, this.onPressed, required this.text, this.textSize = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: 150.0,
      height: textSize * 2,
      child: ElevatedButton(
        onPressed: onPressed,
        style: ElevatedButton.styleFrom(
          primary: sSecBackgroundColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0), side: BorderSide.none),
        ),
        child: Text(
          text,
          style: TextStyle(
            color: sForegroundColor,
            fontSize: textSize,
            fontFamily: 'BalsamiqSans',
          ),
        ),
      ),
    );
  }
}

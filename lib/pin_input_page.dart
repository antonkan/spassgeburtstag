import 'package:flutter/material.dart';
import 'package:spassgeburtstag/answer_input_field.dart';
import 'package:spassgeburtstag/simple_answer_page.dart';
import 'package:spassgeburtstag/simple_question_page.dart';
import 'package:spassgeburtstag/styles.dart';
import 'package:spassgeburtstag/submit_button.dart';

import 'data.dart';

class PinInputPage extends StatelessWidget {
  final Model model;

  const PinInputPage({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: sForegroundColor,
        ),
        title: Text(
          "Spaß Geburtstag",
          style: sAppBarTextStyle,
        ),
        backgroundColor: sSecBackgroundColor,
      ),
      body: PinInputBody(
        model: model,
      ),
    );
  }
}

class PinInputBody extends StatelessWidget {
  final TextEditingController tec = TextEditingController();
  final Model model;

  PinInputBody({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300.0,
              child: Image(
                image: AssetImage('images/logo.png'),
              ),
            ),
            SizedBox(
                width: 250.0,
                child: AnswerInputField(
                  controller: tec,
                  keyboardType: TextInputType.phone,
                  length: 6,
                )),
            SubmitButton(
              text: "Prüfen",
              onPressed: () => onSubmitButton(context),
            )
          ],
        ),
      ),
    );
  }

  void onSubmitButton(BuildContext context) {
    if (model.questions.keys.contains(tec.text)) {
      var questionData = model.questions[tec.text];
      StatelessWidget target;
      if (questionData!.getIsComplete()) {
        target = SimpleAnswerPage(model: model, questionData: questionData);
      } else {
        target = SimpleQuestionPage(model: model, questionData: questionData);
      }
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => target),
      );
    } else {
      showInvalidPinError(context);
    }
    tec.text = "";
  }

  void showInvalidPinError(BuildContext context) {
    AlertDialog alert = AlertDialog(
      title: Text("Ungültige PIN"),
      content: Text("Ups, das war ne falsche PIN, Tschullllllldigung!"),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("OK"),
        )
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

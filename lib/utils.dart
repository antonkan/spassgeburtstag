String simplifyString(String string) {
  return string.replaceAll(RegExp(' '), '').toLowerCase();
}

import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'styles.dart';

class AnswerInputField extends StatelessWidget {
  final TextEditingController controller;
  final TextInputType keyboardType;
  final int length;
  final double sizeFactor;

  const AnswerInputField(
      {Key? key,
      required this.controller,
      required this.length,
      this.sizeFactor = 1.0,
      required this.keyboardType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      backgroundColor: Colors.deepPurple[200],
      length: length,
      obscureText: false,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(5),
        fieldHeight: sizeFactor * 45,
        fieldWidth: sizeFactor * 35,
        activeColor: sSecBackgroundColor,
        inactiveColor: sSecBackgroundColor,
        activeFillColor: Colors.white,
        inactiveFillColor: Colors.white,
        selectedFillColor: Colors.white,
      ),
      textStyle: TextStyle(
        fontFamily: "SyneMono",
        fontSize: sizeFactor * 30.0,
      ),
      animationDuration: Duration(milliseconds: 300),
      controller: controller,
      textCapitalization: TextCapitalization.characters,
      keyboardType: keyboardType,
      enableActiveFill: true,
      onChanged: (String value) {},
      appContext: context,
    );
  }
}

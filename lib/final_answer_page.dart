import 'package:flutter/material.dart';
import 'package:spassgeburtstag/image_with_border.dart';
import 'package:spassgeburtstag/styles.dart';
import 'package:spassgeburtstag/submit_button.dart';

import 'data.dart';

class FinalAnswerPage extends StatelessWidget {
  final Model model;

  const FinalAnswerPage({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: sForegroundColor,
        ),
        title: Text(
          "Spaß Geburtstag",
          style: sAppBarTextStyle,
        ),
        backgroundColor: sSecBackgroundColor,
      ),
      body: FinalAnswerBody(
        model: model,
      ),
    );
  }
}

class FinalAnswerBody extends StatefulWidget {
  final Model model;

  FinalAnswerBody({Key? key, required this.model}) : super(key: key);

  @override
  _FinalAnswerBodyState createState() => _FinalAnswerBodyState();
}

class _FinalAnswerBodyState extends State<FinalAnswerBody> {
  bool buttonDisabled = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      padding: EdgeInsets.all(32.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 60.0,
                  child: Image(
                    image: AssetImage("images/eiffel_tower.png"),
                  ),
                ),
                Column(
                  children: [
                    Text(
                      "Saskia & Anton",
                      style: sFinalTitle,
                    ),
                    Text(
                      "go PARIS",
                      style: sFinalSubtitle,
                    ),
                  ],
                ),
                SizedBox(
                  width: 60.0,
                  child: Image(
                    image: AssetImage("images/eiffel_tower.png"),
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 32.0),
              child: ImageWithBorder(imageUri: widget.model.finalImageUri),
            ),
            SizedBox(
              height: 16.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: 60.0,
                  child: Image(image: AssetImage("images/heart_icon.png")),
                ),
                Visibility(
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          width: 80.0,
                          child: Container(
                            color: sFinalTitleColor,
                            child: Text(
                              "days left",
                              style: sFinalDaysText,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 80.0,
                          child: Container(
                            color: Colors.white,
                            child: Text(
                              widget.model.getDaysLeftUntilTarget().toString(),
                              style: sFinalDaysNumber,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  visible: widget.model.getIsCounterVisible(),
                  replacement: Text(
                    "Allons-y!",
                    style: sDayReachedText,
                  ),
                ),
                SizedBox(
                  width: 60.0,
                  child: Image(image: AssetImage("images/heart_icon.png")),
                ),
              ],
            ),
            Expanded(child: Container()),
            SubmitButton(
              text: "Ergebnisse zurücksetzen",
              textSize: 12.0,
              onPressed: () {
                setState(() {
                  widget.model.resetAllAnswers().then((value) =>
                      Navigator.popUntil(context, (route) => route.isFirst));
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}

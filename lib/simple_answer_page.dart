import 'package:flutter/material.dart';
import 'package:spassgeburtstag/final_question_page.dart';
import 'package:spassgeburtstag/image_with_border.dart';
import 'package:spassgeburtstag/styles.dart';
import 'package:spassgeburtstag/submit_button.dart';

import 'data.dart';

class SimpleAnswerPage extends StatelessWidget {
  final Model model;
  final QuestionData questionData;

  const SimpleAnswerPage(
      {Key? key, required this.model, required this.questionData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: sForegroundColor,
        ),
        title: Text(
          "Spaß Geburtstag",
          style: sAppBarTextStyle,
        ),
        backgroundColor: sSecBackgroundColor,
      ),
      body: SimpleAnswerBody(
        model: model,
        questionData: questionData,
      ),
    );
  }
}

class SimpleAnswerBody extends StatelessWidget {
  final Model model;
  final QuestionData questionData;

  SimpleAnswerBody({Key? key, required this.model, required this.questionData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      padding: EdgeInsets.all(32.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              questionData.successMessage,
              textAlign: TextAlign.center,
              style: sAnswerTextStyle,
            ),
            Text(
              "Hier ist dein Hinweisbild:",
              textAlign: TextAlign.center,
              style: sQuestionTextStyle,
            ),
            SizedBox(height: 16.0),
            ImageWithBorder(imageUri: questionData.answerImageUri),
            SizedBox(height: 16.0),
            SubmitButton(
              text: "Weiter",
              onPressed: () {
                if (model.allAnswersComplete()) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FinalQuestionPage(model: model),
                    ),
                  );
                } else {
                  Navigator.popUntil(context, (route) => route.isFirst);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}

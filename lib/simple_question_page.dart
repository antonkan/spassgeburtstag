import 'package:flutter/material.dart';
import 'package:spassgeburtstag/answer_input_field.dart';
import 'package:spassgeburtstag/image_with_border.dart';
import 'package:spassgeburtstag/simple_answer_page.dart';
import 'package:spassgeburtstag/styles.dart';
import 'package:spassgeburtstag/submit_button.dart';

import 'data.dart';

class SimpleQuestionPage extends StatelessWidget {
  final Model model;
  final QuestionData questionData;

  const SimpleQuestionPage(
      {Key? key, required this.model, required this.questionData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: sForegroundColor,
        ),
        title: Text(
          "Spaß Geburtstag",
          style: sAppBarTextStyle,
        ),
        backgroundColor: sSecBackgroundColor,
      ),
      body: SimpleQuestionBody(
        model: model,
        questionData: questionData,
      ),
    );
  }
}

class SimpleQuestionBody extends StatefulWidget {
  final Model model;
  final QuestionData questionData;

  SimpleQuestionBody(
      {Key? key, required this.model, required this.questionData})
      : super(key: key);

  @override
  _SimpleQuestionBodyState createState() => _SimpleQuestionBodyState();
}

class _SimpleQuestionBodyState extends State<SimpleQuestionBody> {
  final TextEditingController tec = TextEditingController();

  bool _buttonDisabled = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 32.0),
      child: Center(
        child: ListView(
          children: [
            SizedBox(height: 32.0),
            ImageWithBorder(imageUri: widget.questionData.questionImageUri),
            SizedBox(height: 16.0),
            Text(
              widget.questionData.questionText,
              textAlign: TextAlign.center,
              style: sQuestionTextStyle,
            ),
            SizedBox(height: 32.0),
            Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: widget.questionData.correctAnswer.length * 38.0,
                child: AnswerInputField(
                  controller: tec,
                  keyboardType: TextInputType.text,
                  length: widget.questionData.correctAnswer.length,
                  sizeFactor: 0.8,
                ),
              ),
            ),
            SubmitButton(
              text: "Prüfen",
              onPressed: _buttonDisabled
                  ? null
                  : () {
                      if (widget.questionData.isCorrectAnswer(tec.text)) {
                        setState(() {
                          _buttonDisabled = true;
                          widget.questionData.setIsComplete(true).then(
                                (value) => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SimpleAnswerPage(
                                      model: widget.model,
                                      questionData: widget.questionData,
                                    ),
                                  ),
                                ),
                              );
                        });
                      } else {
                        showInvalidAnswerError(context);
                      }
                    },
            )
          ],
        ),
      ),
    );
  }

  void showInvalidAnswerError(BuildContext context) {
    tec.text = "";

    AlertDialog alert = AlertDialog(
      title: Text("Falsche Antwort"),
      content: Text(widget.questionData.wrongAnswerMessage),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("Nochmal"),
        )
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

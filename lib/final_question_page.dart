import 'package:flutter/material.dart';
import 'package:spassgeburtstag/answer_input_field.dart';
import 'package:spassgeburtstag/image_with_border.dart';
import 'package:spassgeburtstag/styles.dart';
import 'package:spassgeburtstag/submit_button.dart';

import 'data.dart';
import 'final_answer_page.dart';

class FinalQuestionPage extends StatelessWidget {
  final Model model;

  const FinalQuestionPage({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: sForegroundColor,
        ),
        title: Text(
          "Spaß Geburtstag",
          style: sAppBarTextStyle,
        ),
        backgroundColor: sSecBackgroundColor,
      ),
      body: FinalQuestionBody(
        model: model,
      ),
    );
  }
}

class FinalQuestionBody extends StatefulWidget {
  final Model model;

  FinalQuestionBody({Key? key, required this.model}) : super(key: key);

  @override
  _FinalQuestionBodyState createState() => _FinalQuestionBodyState();
}

class _FinalQuestionBodyState extends State<FinalQuestionBody> {
  final TextEditingController tec = TextEditingController();

  bool buttonDisabled = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: sBackgroundColor,
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 32.0),
      child: Center(
        child: ListView(
          children: [
            SizedBox(height: 16.0),
            Text(
              "Sehr gut, du hast alle 4 Fragen korrekt beantworten können!",
              textAlign: TextAlign.center,
              style: sAnswerTextStyle,
            ),
            SizedBox(height: 8.0),
            Text(
              "Dein Lösungswort ist die Gemeinsamkeit aller 4 Bilder...",
              textAlign: TextAlign.center,
              style: sQuestionTextStyle,
            ),
            SizedBox(height: 16.0),
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Flexible(
                        child: ImageWithBorder(
                          imageUri: widget.model.questions.values
                              .elementAt(0)
                              .answerImageUri,
                        ),
                      ),
                      SizedBox(width: 8.0),
                      Flexible(
                        child: ImageWithBorder(
                          imageUri: widget.model.questions.values
                              .elementAt(1)
                              .answerImageUri,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    children: [
                      Flexible(
                        child: ImageWithBorder(
                          imageUri: widget.model.questions.values
                              .elementAt(2)
                              .answerImageUri,
                        ),
                      ),
                      SizedBox(width: 8.0),
                      Flexible(
                        child: ImageWithBorder(
                          imageUri: widget.model.questions.values
                              .elementAt(3)
                              .answerImageUri,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 32.0,
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: SizedBox(
                      width: widget.model.finalAnswer.length * 50.0,
                      child: AnswerInputField(
                        controller: tec,
                        keyboardType: TextInputType.text,
                        length: widget.model.finalAnswer.length,
                        sizeFactor: 1.3,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: SubmitButton(
                text: "Prüfen",
                onPressed: buttonDisabled
                    ? null
                    : () {
                        if (widget.model.finalAnswerCorrect(tec.text)) {
                          setState(() {
                            widget.model
                                .setFinalQuestionComplete(true)
                                .then((value) => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => FinalAnswerPage(
                                            model: widget.model),
                                      ),
                                    ));
                          });
                        } else {
                          showInvalidAnswerError(context);
                        }
                      },
              ),
            )
          ],
        ),
      ),
    );
  }

  void showInvalidAnswerError(BuildContext context) {
    tec.text = "";

    AlertDialog alert = AlertDialog(
      title: Text("Falsche Antwort"),
      content: Text("...Überleg weiter ;)"),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("Nochmal"),
        )
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

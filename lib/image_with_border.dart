import 'package:flutter/material.dart';

class ImageWithBorder extends StatelessWidget {
  final String imageUri;

  const ImageWithBorder({Key? key, required this.imageUri}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 3.0,
          ),
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          child: Image(
            image: AssetImage(imageUri),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

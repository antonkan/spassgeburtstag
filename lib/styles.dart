import 'package:flutter/material.dart';

Color sBackgroundColor = Colors.purple[200]!;
Color sForegroundColor = Colors.deepPurple[700]!;
Color sSecBackgroundColor = Color(0xFFFFD700);
Color sFinalTitleColor = Colors.blue[600]!;
Color sFinalSubtitleColor = Color(0xFFFFD700);
Color sDayReachedTextColor = Colors.pink[700]!;

TextStyle sAppBarTextStyle = TextStyle(
  fontFamily: "Caveat",
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
  color: sForegroundColor,
);

TextStyle sQuestionTextStyle = TextStyle(
  fontFamily: "Caveat",
  fontSize: 26.0,
);

TextStyle sAnswerTextStyle = TextStyle(
  fontFamily: "Caveat",
  fontWeight: FontWeight.bold,
  fontSize: 26.0,
);

TextStyle sFinalTitle = TextStyle(
  fontFamily: "AmaticSC",
  fontWeight: FontWeight.bold,
  fontSize: 40.0,
  color: sFinalTitleColor,
);

TextStyle sFinalSubtitle = TextStyle(
  fontFamily: "AmaticSC",
  fontWeight: FontWeight.bold,
  fontSize: 54.0,
  color: sFinalSubtitleColor,
);

TextStyle sFinalDaysText = TextStyle(
  fontFamily: "AmaticSC",
  fontWeight: FontWeight.bold,
  fontSize: 28.0,
  color: sFinalSubtitleColor,
);

TextStyle sFinalDaysNumber = TextStyle(
  fontFamily: "AmaticSC",
  fontWeight: FontWeight.bold,
  fontSize: 50.0,
  color: Colors.black,
);

TextStyle sDayReachedText = TextStyle(
  fontFamily: "AmaticSC",
  fontWeight: FontWeight.bold,
  fontSize: 48.0,
  color: sDayReachedTextColor,
);
